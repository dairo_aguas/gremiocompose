package dairo.aguas.merqueocompose.data

import dairo.aguas.merqueocompose.domain.models.Product

/**
 * Created by Dairo Aguas B on 26/01/2022.
 */
class MockDataProvider {

    companion object {
        fun getProducts(): List<Product> = listOf(
            Product(
                id = 0,
                name = "Banano x Bolsa (5-7 Und Aprox)",
                description = "El banano es una fuente importante de carbohidratos, dentro de los cuales destaca el almidón y azúcares como sacarosa, glucosa y fructosa en el banano maduro.",
                weight = 1,
                unit = "Kg",
                price = 3188.0,
                image = "https://static.merqueo.com/images/products/large/c6d7e4fd-d250-4051-b313-ea73c89ee500.jpg"
            ),
            Product(
                id = 1,
                name = "Aguacate Hass x Malla (4 a 7 Und)",
                description = "El aguacate, además de ser famoso por acompañar gran parte de las ensaladas, es un alimento muy nutritivo. Los beneficios del aguacate son muchos. Es una fuente de grasas sanas, vitaminas y minerales.",
                weight = 900,
                unit = "Gr",
                price = 6460.0,
                image = "https://static.merqueo.com/images/products/large/ab74626a-9f45-4b31-8da2-a585fee35e19.jpg"
            ),
            Product(
                id = 2,
                name = "Piña Golden x Unidad",
                description = "La piña tiene antioxidantes, vitaminas, minerales y otras ventajas saludables.",
                weight = 1,
                unit = "Kg",
                price = 4123.0,
                image = "https://static.merqueo.com/images/products/large/223ae3e6-1809-499e-b9f1-1a4844f5c82a.jpg"
            ),
            Product(
                id = 3,
                name = "Naranja Jugo x Malla",
                description = "Las naranjas para zumo son de menor tamaño y mayor producción de zumo, tienen menos pulpa, asi como una piel más fina. El zumo de naranja si se toma en ayunas, elimina sustancias tóxicas y nos previene contra muchas enfermedades. El zumo de naranja es más nutritivo con su pulpa debido a la existencia de flavonoides en esta.",
                weight = 5,
                unit = "Kg",
                price = 8750.0,
                image = "https://static.merqueo.com/images/products/large/af9f6fa7-46c4-42e7-b3bf-7c8463fe12f9.jpg"
            ),
            Product(
                id = 4,
                name = "Lulo x Bandeja (4 a 6 Und)",
                description = "El lulo es una planta semisilvestre que crece en ecosistemas abiertos por el hombre, especialmente en sitios frescos, sombreados y con buena humedad (áreas de sotobosque en las partes bajas del bosque primario), bajo estas condiciones, la planta es exuberante, muy verde y vigorosa.",
                weight = 600,
                unit = "Gr",
                price = 5012.0,
                image = "https://static.merqueo.com/images/products/large/c2e310ef-2a41-4a81-83a3-ebe7f55e2339.jpg"
            ),
            Product(
                id = 5,
                name = "Zanahoria x Paquete (3-4 Und Aprox)",
                description = "La zanahoria es una de las hortalizas más producidas y consumidas en el mundo,contribuye a la salud de la piel y la vista, es rica en vitamina A, B3, E y K, minerales  como el potasio, el fósforo, el magnesio, el yodo y el calcio.",
                weight = 500,
                unit = "Gr",
                price = 1995.0,
                image = "https://static.merqueo.com/images/products/large/a1ed15c5-a25d-4014-bfc9-fbf46d991ff0.jpg"
            )
        )

        fun getProductById(id: Int): Product? =
            getProducts().find { item -> item.id == id }
    }
}
