package dairo.aguas.merqueocompose.ui.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import coil.compose.rememberImagePainter
import dairo.aguas.merqueocompose.data.MockDataProvider
import dairo.aguas.merqueocompose.domain.models.Product
import dairo.aguas.merqueocompose.ui.components.CustomAppBar
import dairo.aguas.merqueocompose.ui.components.ProductCardHorizontal
import dairo.aguas.merqueocompose.ui.theme.MerqueoComposeTheme

/**
 * Created by Dairo Aguas B on 26/01/2022.
 */
@Composable
fun DetailScreen(navController: NavController, product: Product) {
    Scaffold(
        topBar = {
            CustomAppBar(title = "Producto") {
                navController.navigateUp()
            }
        },
        content = {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier.verticalScroll(rememberScrollState())
            ) {
                Card(elevation = 4.dp) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier.padding(10.dp)
                    ) {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(300.dp)
                        ) {
                            Image(
                                painter = rememberImagePainter(data = product.image),
                                contentDescription = null,
                                modifier = Modifier.fillMaxSize()
                            )
                        }
                        Text(
                            text = product.name,
                            style = MaterialTheme.typography.h1
                        )
                        Text(
                            text = "${product.weight} ${product.unit}",
                            style = MaterialTheme.typography.body1,
                            modifier = Modifier.padding(top = 5.dp)
                        )
                        Text(
                            text = "$ ${product.price}",
                            style = MaterialTheme.typography.h2,
                            modifier = Modifier.padding(top = 5.dp)
                        )
                    }
                }
                Spacer(modifier = Modifier.height(16.dp))
                Card(elevation = 4.dp) {
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .size(300.dp)
                            .padding(16.dp)
                    ) {
                        Text(
                            text = "Productos sugeridos",
                            style = MaterialTheme.typography.h2
                        )
                        LazyRow {
                            items(MockDataProvider.getProducts()) { product ->
                                ProductCardHorizontal(product = product)
                            }
                        }
                    }
                }
                Spacer(modifier = Modifier.height(16.dp))
                Card(elevation = 4.dp) {
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(16.dp)
                    ) {
                        Text(
                            text = "Detalle",
                            style = MaterialTheme.typography.h2
                        )
                        Text(
                            text = product.description,
                            style = MaterialTheme.typography.body1
                        )
                    }
                }
            }
        }
    )
}

@Preview(showBackground = true)
@Composable
fun DetailScreenPreview() {
    val navController = rememberNavController()
    val product = MockDataProvider.getProductById(0)
    MerqueoComposeTheme {
        DetailScreen(navController, product!!)
    }
}
