package dairo.aguas.merqueocompose.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import dairo.aguas.merqueocompose.data.MockDataProvider
import dairo.aguas.merqueocompose.domain.models.Product
import dairo.aguas.merqueocompose.ui.theme.MerqueoComposeTheme

/**
 * Created by Dairo Aguas B on 26/01/2022.
 */

@Composable
fun ProductCardHorizontal(product: Product) {
    Card(
        modifier = Modifier
            .size(width = 200.dp, height = 300.dp)
            .padding(8.dp),
        elevation = 10.dp,
        shape = MaterialTheme.shapes.small
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier.padding(16.dp)
        ) {
            Image(
                painter = rememberImagePainter(data = product.image),
                contentDescription = null,
                modifier = Modifier.size(100.dp)
            )
            Text(
                text = product.name
            )
            Text(
                text = "${product.weight} ${product.unit}",
                style = MaterialTheme.typography.h1
            )
            Text(
                text = "$ ${product.price}",
                style = MaterialTheme.typography.h1
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ProductCardHorizontalPreview() {
    val product = MockDataProvider.getProductById(0)
    MerqueoComposeTheme {
        ProductCardHorizontal(product!!)
    }
}
