package dairo.aguas.merqueocompose.ui.screens

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import dairo.aguas.merqueocompose.data.MockDataProvider
import dairo.aguas.merqueocompose.ui.components.CustomAppBar
import dairo.aguas.merqueocompose.ui.components.ProductCard
import dairo.aguas.merqueocompose.ui.theme.MerqueoComposeTheme

/**
 * Created by Dairo Aguas B on 26/01/2022.
 */
@Composable
fun HomeScreen(navController: NavController) {
    val products = MockDataProvider.getProducts()
    Scaffold(
        topBar = {
            CustomAppBar(title = "Frutas y verduras")
        },
        content = {
            LazyColumn {
                items(products) { product ->
                    ProductCard(product = product) {
                        navController.navigate("detail/${product.id}") {
                            launchSingleTop = true
                        }
                    }
                }
            }
        }
    )
}

@Preview(showBackground = true)
@Composable
fun HomeScreenPreview() {
    val navController = rememberNavController()
    MerqueoComposeTheme {
        HomeScreen(navController)
    }
}
