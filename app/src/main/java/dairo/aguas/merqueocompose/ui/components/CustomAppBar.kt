package dairo.aguas.merqueocompose.ui.components

import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import dairo.aguas.merqueocompose.ui.theme.MerqueoComposeTheme

/**
 * Created by Dairo Aguas B on 26/01/2022.
 */
@Composable
fun CustomAppBar(
    title: String,
    navigationAction: (() -> Unit)? = null
) {
    if (navigationAction != null) {
        TopAppBar(
            title = { Text(text = title) },
            navigationIcon = {
                IconButton(
                    onClick = { navigationAction() }
                ) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = null
                    )
                }
            },
            backgroundColor = MaterialTheme.colors.primary
        )
    } else {
        TopAppBar(
            title = { Text(text = title) },
            backgroundColor = MaterialTheme.colors.primary
        )
    }
}

@Preview(showBackground = true)
@Composable
fun CustomAppBarPreview() {
    MerqueoComposeTheme {
        CustomAppBar("Frutas y verduras")
    }
}
