package dairo.aguas.merqueocompose.ui.theme

import androidx.compose.ui.graphics.Color

val Pink400 = Color(0xFFD2287E)
val Pink500 = Color(0xFFD0006F)
val Pink800 = Color(0xFF55002D)
val Teal200 = Color(0xFF03DAC5)