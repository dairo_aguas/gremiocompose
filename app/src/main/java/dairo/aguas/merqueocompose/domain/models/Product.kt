package dairo.aguas.merqueocompose.domain.models

/**
 * Created by Dairo Aguas B on 26/01/2022.
 */
data class Product(
    val id: Int,
    val name: String,
    val description: String,
    val weight: Int,
    val unit: String,
    val price: Double,
    val image: String
)
