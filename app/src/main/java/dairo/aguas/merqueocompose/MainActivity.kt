package dairo.aguas.merqueocompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import dairo.aguas.merqueocompose.data.MockDataProvider
import dairo.aguas.merqueocompose.ui.screens.DetailScreen
import dairo.aguas.merqueocompose.ui.screens.HomeScreen
import dairo.aguas.merqueocompose.ui.theme.MerqueoComposeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MerqueoComposeTheme {
                Surface(color = MaterialTheme.colors.background) {
                    NavigationHost()
                }
            }
        }
    }
}

@Composable
fun NavigationHost() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "home") {
        composable(route = "home") {
            HomeScreen(navController = navController)
        }
        composable(route = "detail/{productId}") { navBackStackEntry ->
            val productId = navBackStackEntry.arguments?.getString("productId") ?: "0"
            DetailScreen(
                navController = navController,
                product = MockDataProvider.getProductById(productId.toInt())!!
            )
        }
    }
}
